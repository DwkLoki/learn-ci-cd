const express = require('express')
const app = express.Router()

app.get('/', async (req, res) => {
    res.send("hello from auth API")
})

module.exports = app